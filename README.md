# Quantum-Computing-Opportunities
A list of hands-on opportunites (not jobs) in quantum computing


## Internships
| Name | Link | Organization | Date | Status | Feedback |
| :--: | :--: | :--: | :--: | :--: | :--: |
| BNL SULI | [Home page](https://www.bnl.gov/education/programs/program.php?q=188) | Brookhaven National Lab | June 7 - August 13, 2021 | ![open status][open] | None yet |
| Horizon Summer Internship | [Scroll Down](http://horizonquantum.com/) | Horizon Quantum Computing | Summer | ![closed status][closed] | [![FB][feedback]](FEEDBACK.md#summer-internship-horizon-quantum-computing) |
| IBM Summer Internships | [Blog](https://www.ibm.com/blogs/research/2020/09/2021-ibmquantum-internships/) | IBM | Summer | ![open status][open] | None yet |
| Ketita Internships | [Internship Block](https://ketita-labs.com/#block-intern) | Ketita Labs | ??? | ![open status][open] | [![FB][feedback]](FEEDBACK.md#ketita-labs-internships) |
| ML4Q | [Program homepage](https://ml4q.de/ml4q-internship/) | ML4Q | May-Sep | ![open status][open] | None yet |
| QURIP | [Info Page](https://www.ibm.com/quantum-computing/internship/qurip/) | IBM and Princeton| Summer | ![closed status][closed] | [![FB][feedback]](FEEDBACK.md#qurip-ibm-and-princeton) |
| Riverlane Internships | [Job posting](https://www.riverlane.com/vacancy/quantum-computing-summer-internship-scheme-for-undergraduate-masters-and-phd-students/) | Riverlane | Jun-Sep 2021 | ![open status][open] | None yet |
| Xanadu Residency | [Program homepage](https://residency.xanadu.ai/) | Xanadu | May-Aug 2021 | ![open status][open] | None yet |
| Zapata QC Research Internships | [Position Description](https://zapata.bamboohr.com/jobs/view.php?id=40&source=bamboohr) | Zapata Computing | None | ![closed status][closed] | None yet |
| Zapata Quantum Platform Internships | [Position Description](https://zapata.bamboohr.com/jobs/view.php?id=68&source=bamboohr) | Zapata Computing | Summer 2021 | ![open status][open] | None yet |


## Research Opportunities
| Name | Link | Organization | Date | Status | Feedback |
| :--: | :--: | :--: | :--: | :--: | :--: |
| URA | [Info Page](https://uwaterloo.ca/institute-for-quantum-computing/ura) | University of Waterloo IQC | Summer | ![closed status][closed] | [![FB][feedback]](FEEDBACK.md#ura-university-of-waterloo-iqc) |


## Programs
| Name | Link | Organization | Date | Status | Feedback |
| :--: | :--: | :--: | :--: | :--: | :--: |
| Los Alamos Quantum Computing Summer School| [Archive of Info Page](https://web.archive.org/web/20201126221938/https://www.lanl.gov/projects/national-security-education-center/information-science-technology/summer-schools/quantumcomputing/index.php) | Information Science and Technology Institute (ISTI) | Summer (10 weeks) | ![open status][open] | [![FB][feedback]](FEEDBACK.md#los-alamos-quantum-computing-summer-school-isti) |
| QCSYS | [Info Page](https://uwaterloo.ca/institute-for-quantum-computing/programs/qcsys) | University of Waterloo IQC | August | ![closed status][closed] | [![FB][feedback]](FEEDBACK.md#qcsys-university-of-waterloo-iqc) |
| Qiskit Global Summer School | [Info Page](https://www.eventbrite.com/e/qiskit-global-summer-school-tickets-110201256926) | IBM | July 20-31 | ![closed status][closed] | [![FB][feedback]](FEEDBACK.md#qiskit-global-summer-school) |
| QOSF Mentorship program | [Info Page](https://qosf.org/qc_mentorship/) | Quantum Open Source Foundation | Program runs in batches | ![open status][open] | [![FB][feedback]](FEEDBACK.md#qosf-mentorship-program-quantum-open-source-foundation) |
| Summer School on Machine Learning and Big Data with Quantum Computing | [Info Page](https://smbq2020.dcc.fc.up.pt/)| University of Porto, Portugal | September 7-8 | ![closed status][closed] | [![FB][feedback]](FEEDBACK.md#smbq2020---university-of-porto) |
| USEQIP | [Info Page](https://uwaterloo.ca/institute-for-quantum-computing/programs/useqip) | University of Waterloo IQC | May-June | ![closed status][closed] | [![FB][feedback]](FEEDBACK.md#useqip-university-of-waterloo-iqc) |



## Contributing

Step 1: [![Contributing Guidelines](https://img.shields.io/badge/contribute-read%20the%20guidelines!-informational?style=flat-square)](CONTRIBUTING.md)

Step 2: [![GitHub forks](https://img.shields.io/badge/fork-this%20repo-blue?style=flat-square)](https://gitlab.com/1ethanhansen/Quantum-Computing-Opportunities/-/forks/new)

Step 3: Done!

[open]: https://img.shields.io/badge/status-open-brightgreen?style=flat-square
[closed]: https://img.shields.io/badge/status-closed-red?style=flat-square
[feedback]: https://img.shields.io/badge/feedback-here!-informational?style=flat-square
